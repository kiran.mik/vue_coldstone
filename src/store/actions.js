import axios from 'axios';
const API_URL="https://biteheist.com/ordering/tx/GoodEatsDEV/index.php?"


export const GET_TOKEN ='get_token'
export const getToken =(callback) => async dispatch =>
{
  var response= await axios({
    method :'post',
    url: API_URL+'route=feed/rest_api/gettoken&grant_type=client_credentials',
    headers :
    {
      'Authorization':'Basic b2F1dGhfY2xpZW50MTIzJDpvYXV0aF9zZWNyZXQ1NDYk'
    },
   })
   localStorage.setItem("access_token",response.data.access_token)
   let payload = { access_token : response.data.access_token}
    dispatch({ type: GET_TOKEN,payload });
    callback(response)
}



export const FETCH_SIGNIN = 'fetch_signin';
 export const fetchSignin = (user,callback) => async dispatch => {
 
   const token = await localStorage.getItem("access_token");
   console.log("here",token)
   var bodyFormData = new FormData();
   bodyFormData.set('email', user.email);
   bodyFormData.set('password', user.password);
   try {
    let response = await axios({
    method :'post',
    url:API_URL+'route=rest/login/login',
    headers: {
     'Content-Type': 'multipart/form-data',
     'Authorization': "Bearer "+ token
   },
     data: bodyFormData,
    })
   
    let {data}=response;
    console.log(response)
    if(data.success == true){
      localStorage.getItem('access_token');
      localStorage.setItem("email",data.data.email)
      let payload = {
        userdetails:data.email,
        auth:data.success,
        response:response.data
      }
      dispatch({ type: FETCH_SIGNIN,payload });
    }
     callback(response)
    } catch (error) {
      throw error;
    }
  };
