import Vue from 'vue'
import Router from 'vue-router'
import order from '@/components/order'
import chooseLocation from '@/components/chooseLocation'
import signIn from '../components/signInout'
import page from '../components/page'
import orderNow from '../components/orderNow'
import menuCatering from '../components/menuCatering'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'order',
      component: order,
      exact: true
    },
    {
      path: '/chooseLocation',
      name: 'chooseLocation',
      component: chooseLocation
    },
    {
      path: '/signin',
      name: 'signIn',
      component: signIn
    },
    {
      path: '/page',
      name: 'page',
      component: page
    },
    {
      path: '/orderNow',
      name: 'orderNow',
      component: orderNow
    },
    {
      path: '/menuCatering',
      name: 'menuCatering',
      component: menuCatering
    },
  ]
})
